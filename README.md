# c7-conda-lgarciac-cuda92

CentOS-7 based container (can be run on tars) without "kernel too old" issue
with miniconda installed and configured with CUDA-9.2 for gpu acceleration
(can be used on all the K80/M4 and P100 gpus nodes)

Tested with singularity 3.5.2

There is a prebuilt version at ~tru/singularity.d/containers/c7-conda-lgarciac-cuda92-2020-04-15-1840.sif

Building your own after adapting the code (on your own resources, you need to have "root" privileges):
```
sudo singularity build c7-conda-lgarciac-cuda92.sif Singularity
```

Transfer to tars (scp/sftp/rsync/...) and run with:
```
$ module add singularity/3.5.2
$ srun --qos gpu --gres=gpu:1 -p gpu -n1 -N1 --pty --preserve-env singularity exec -B /pasteur --nv ~tru/singularity.d/containers/c7-conda-lgarciac-cuda92-2020-04-15-1840.sif  python3 ~tru/python/torch-cuda.py 
```

Adapt with the number of cores,  gpu type (K80|M40|P100),...

